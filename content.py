#!/usr/bin/python3

# HAR File analysieren und Statistik ueber Inhalte (Sites) zeigen
# ---------------------------------------------------------------

# Module laden
from sys import argv
import json
from haralyzer import HarParser, HarPage
from urllib.parse import urlparse

# Kommandozeile pruefen
infiles=[]
if len(argv) > 1:
	# erstes Arg ist Scriptname
	argv.pop(0)
	# alle anderen sind Input Files
	infiles=argv
else:
	# array
	infiles.append('stackoverflow.com.har')

# Statistik
domainsSeen={}
allDomains={}

# HAR Datei oeffnen und parsen
for infile in infiles:
	try:
		with open(infile,'r') as f:
			harData = json.loads(f.read())
			if len(harData['log']['pages']) == 1:
				# liefert page_X
				page=harData['log']['pages'][0]['id']
				#print("Found page",page)
			else:
				print("Ooops, expecting one page only.")
				exit(1)
			# Seite parsen
			harPage = HarPage(page, har_data=harData)
	except:
		print("Ooops, something went wrong with '%s'. Is this a HAR file?" % infile)
		exit(1)

	# neue Site merken und Statistik vorbereiten
	thisSite = urlparse(harPage.url).netloc
	domainsSeen[thisSite] = {}

	# Aus dem HTTP Request die FQDN rausziehen und merken
	for e in harPage.entries:
		# fqdn trennen, in array speichern
		where = urlparse(e.request.url).netloc.split(".")
		# reduzieren auf second level domain, ausser '.uk' (.co.uk)
		if where[-1] != 'uk':
			while len(where)>2:
				# erstes Element verwerfen
				where.pop(0)
		else:
			while len(where)>3:
				# erstes Element verwerfen
				where.pop(0)
		# wieder mit Punkt zusammen fuegen
		dom='.'.join(where)

		# Hits zaehlen
		if domainsSeen[thisSite].get(dom) == None:
			domainsSeen[thisSite][dom] = 0
		domainsSeen[thisSite][dom] += 1

		# Liste aller jemals gesehenen Domains
		if allDomains.get(dom) == None:
			allDomains[dom] = 0
		allDomains[dom] += 1

# Zusammenfassung
for site in domainsSeen.keys():
	print("-"*76)
	print(site, "is embedding from:")
	print("-"*76)
	for source in sorted(domainsSeen[site].keys()):
		print("%s\t%u" % (source, domainsSeen[site][source]))

# Tabelle mit allen Sites
print("-"*76)
print("%32s" % "Site:", end="\t")
for site in sorted(domainsSeen.keys()):
	print(site, end="\t")
print()

for dom in sorted(allDomains.keys()):
	print("%32s" % dom, end="\t")
	for site in sorted(domainsSeen.keys()):
		if dom in domainsSeen[site]:
			print("X", end="\t")
		else:
			print(".", end="\t")
	print()

print("-"*76)
