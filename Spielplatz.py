#!/usr/bin/python3

from Monster import *

monster = [
	Erdbeerli( 5, 5), Erdbeerli( 7, 5), Erdbeerli(14, 5), Erdbeerli(20, 5),
	Erdbeerli( 8, 6), Erdbeerli(19, 6),
	Erdbeerli( 7, 7), Erdbeerli( 8, 7), Erdbeerli(15, 7),
	Erdbeerli(17, 8),
	Erdbeerli( 4, 9), Erdbeerli(17, 9),
	Erdbeerli( 2,10),
	Erdbeerli(20,11),
	Erdbeerli( 1,13), Erdbeerli( 7,13), Erdbeerli(14,13),
	Erdbeerli( 4,14), Erdbeerli( 8,14),
	Erdbeerli(17,15),
	Erdbeerli( 6,16), Erdbeerli(15,16),
	Erdbeerli(13,17),
	Erdbeerli(13,18), Erdbeerli(17,18),
	Erdbeerli(15,19), Erdbeerli(16,19),

	Pirati( 5, 1), Pirati(16, 1),
	Pirati( 0, 4), Pirati( 7, 4),
	Pirati( 8, 5), Pirati(17, 5),
	Pirati( 0, 7),
	Pirati( 1,14), Pirati( 6,14),
	Pirati( 4,17), Pirati( 8,17),
	Pirati( 8,18),
	Pirati(17,19),
	Pirati( 7,20),

	Fressi( 0, 0), Fressi(19, 0),
	Fressi( 1, 1),
	Fressi( 4, 3),
	Fressi(14,10),
	Fressi( 1,12),
	Fressi( 5,13),
	Fressi( 1,16), Fressi(13,16), Fressi(14,16), Fressi(20,16),
	Fressi(20,18),
	Fressi( 1,19),

	Brummi( 6, 0), Brummi(12, 0), 
	Brummi( 3, 1),
	Brummi( 7, 3),
	Brummi( 4, 4),
	Brummi(10, 5), Brummi(12, 5), Brummi(16, 5),
	Brummi( 1, 6), Brummi(10, 6),
	Brummi( 9, 8), Brummi(12, 8),
	Brummi( 1, 9),
	Brummi( 4,16), Brummi( 8,16),
	Brummi( 0,17), Brummi(10,17), Brummi(12,17),
	Brummi( 9,19),

	Glubschi( 9, 1),
	Glubschi( 9, 3),
	Glubschi(17, 4),
	Glubschi( 1, 7), Glubschi( 2, 7),
	Glubschi(11, 9),
	Glubschi( 9,10), Glubschi(15,10),
	Glubschi(12,12),
	Glubschi( 0,13),

	Keksi(15, 4),
	Keksi( 3, 6), Keksi(13, 6), Keksi(15, 6),
	Keksi( 4, 8), Keksi(19, 8),
	Keksi( 7, 9), Keksi(12, 9),
	Keksi( 0,10), Keksi(18,10),
	Keksi( 5,12),
	Keksi(12,14), Keksi(18,14),
	Keksi( 4,15),
	Keksi( 7,17),
	Keksi( 5,18), Keksi( 6,18), Keksi(19,18),
]

# -- main --
matrix=['.']*21*21

print(len(monster), "Monster!")

for m in monster:
	m.jump(10)
	#print(m.pos(), end="  ")
	[x,y] = m.pos()
	matrix[y*21+x] = 'X'

# Spaltenueberschrift
print("   ",end="")
for x in range(1, 22):
	print(x%10,end="")
print()

for y in range(0, 21):
	# Zeilennummer
	print("%2u" % (y+1), end=" ")
	for x in range(0, 21):
		print(matrix[y*21+x],end="")
	print()

