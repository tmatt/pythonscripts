from math import sqrt

class Primes:

	def __init__(self):
		self.knownprimes=[2,3,5,7,11]
		self.hiprime=self.knownprimes[-1]
		self.debug=False

	def __del__(self):
		if self.debug == True:
			print('*destructor*')

	# Probedivision, optimiert
	# Ermittelt erst alle Primzahl-Divisoren und testet dann Division mit diesen
	# --------------------------------------------------------------------------

	def isPrime(self,n):
		# groesste Zahl fuer Probedivision
		upper = 1+int(sqrt(n))
		if self.debug == True:
			print("%u, upper %u, hiprime %u, elements %u" %
				(n, upper,self.hiprime,len(self.knownprimes)))

		# Zuerst potentielle Divisoren auf Prim testen. hiprime ist die letzte
		# bekannte Primzahl, damit ist hiprime+2 ist die potentiell naechste
		# Primzahl
		if upper >= (self.hiprime+2):
			if self.debug == True:
				print("testing for new prime divisors between %u .. %u" %
					(self.hiprime+2, upper+1))
			# range(a,b) <- b exklusive!
			for d in range(self.hiprime+2,upper+1,2):
				# ist dieser Divisor eine Primzahl?
				if self.testForPrime(d) == True:
					self.knownprimes.append(d)
					self.hiprime=d

		# Nur die bekannten Prim-Divisoren testen
		for d in self.knownprimes:
			if self.debug == True:
				print(d,end=" ")
			# schon alle durch? dann ist n prim
			if d > upper:
				return True
			if n % d == 0:
				if self.debug == True:
					print("<-- %ux%u=%u" % (d,n/d,n*d))
				return False


		# Alle durch: neue Primzahl gefunden!
		# TBD: diese auch merken? Wie/wo?
		return True

	# Brute-Force Probedivision
	# Aufruf nur intern von isPrime, um die Primzahl-Divisoren zu ermitteln
	# ---------------------------------------------------------------------

	def testForPrime(self,n):
		# gerade Zahlen sind keine Primzahlen, da durch 2 teilbar
		if n & 1 == 0:
			return False
		# groesste Zahl fuer Probedivision
		upper = 1+int(sqrt(n))
		for d in range(3,upper,2):
			# Rest?
			if n % d == 0:
				return False
		# alle Divisionen hatten Rest
		return True

