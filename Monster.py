class Monster:

	def __init__(self,x,y):
		# Startposition
		self.x=x
		self.y=y
		# Sprung
		self.j=0
		# Debug
		self.debug=False

	def jump(self,j):
		for loop in range(1,j+1):
			if self.debug == True:
				print("Position alt %2u:%2u" % (self.x, self.y))
				print("     Springe %2u:%2u" % (self.dx[self.j], self.dy[self.j]))
			# Springen
			self.x += self.dx[self.j]
			self.y += self.dy[self.j]
			# Position anpassen wenn Rand erreicht
			if self.debug == True:
				print("Position roh %2u:%2u" % (self.x, self.y))
			[self.x, self.y] = self.pos()
			if self.debug == True:
				print("Position neu %2u:%2u" % (self.x, self.y))
			# naechster Sprung
			self.j += 1
			self.j %= 3

	def pos(self):
		# Spielplatz 21x21
		if self.x < 0:
			self.x = 21 - abs(self.x)
		if self.y < 0:
			self.y = 21 - abs(self.y)
		return [self.x % 21, self.y % 21]		

class Erdbeerli(Monster):

	def __init__(self,x,y):
		super().__init__(x,y)
		# Spruenge horizontal
		self.dx=[+1, +1, +1]
		# Spruenge vertikal
		self.dy=[-2, +2,  0]

class Pirati(Monster):

	def __init__(self,x,y):
		super().__init__(x,y)
		# Spruenge horizontal
		self.dx=[+1, +1, +1]
		# Spruenge vertikal
		self.dy=[-2, +1, -2]

class Brummi(Monster):

	def __init__(self,x,y):
		super().__init__(x,y)
		# Spruenge horizontal
		self.dx=[-1, +3, -1]
		# Spruenge vertikal
		self.dy=[+2, -1, +1]

class Fressi(Monster):

	def __init__(self,x,y):
		super().__init__(x,y)
		# Spruenge horizontal
		self.dx=[-1, -1, -1]
		# Spruenge vertikal
		self.dy=[-1, +2, -3]

class Glubschi(Monster):

	def __init__(self,x,y):
		super().__init__(x,y)
		# Spruenge horizontal
		self.dx=[-1,  0, -1]
		# Spruenge vertikal
		self.dy=[-1, +3, -1]

class Keksi(Monster):

	def __init__(self,x,y):
		super().__init__(x,y)
		# Spruenge horizontal
		self.dx=[-1, -1, -1]
		# Spruenge vertikal
		self.dy=[-1, +2, -1]

