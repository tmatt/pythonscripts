#!/usr/bin/python3

# -- Standardbibliotheken einbinden ----------------------------------------
from random import random
from math   import sqrt
import time
import sys

# -- Eigene Bibliotheken einbinden -----------------------------------------
from Primes import Primes

# -- Funktionen ------------------------------------------------------------
def qsumme(n):
	q=0
	n_str=str(n)
	for i in range(len(n_str)):
		q += int(n_str[i])
	return q

def isprime(n):
	# durch 2 teilbar (d.h. gerade Zahl)
	if n & 1 == 0:
		return 2

	# durch 3 teilbar, wenn Quersumme durch 3 teilbar ist
	# --scheint nicht effizienter zu sein als eine Division--
	#if qsumme(n) % 3==0:
	#	return 3

	# restliche ausprobieren, nur ungerade Divisoren
	# obere Grenze ist Wurzel(n)
	for f in range(3,1+int(sqrt(n)),2):
		if (n % f)==0:
			#print(n,"ist teilbar durch",f)
			return f
	#print(n,"ist eine Primzahl!")
	return n

# -- main ------------------------------------------------------------------
# Kommandozeile auswerten
if len(sys.argv) == 3:
	lower = int(sys.argv[1])
	upper = int(sys.argv[2])
	if upper <= lower:
		print("Synopsis:")
		print("\t",sys.argv[0],"[lower] [upper]")
		print("Description:")
		print("\tFind all Primes between [lower] and [upper]")
		exit()
else:
	lower = 8100000
	upper = 8200000

print("Ermittle alle Primzahlen aus dem Zahlenbereich",lower,"-",upper)
print("-"*76)

# Durchlauf mit Function oben
primes=[]
startt=time.process_time()
for p in range(lower,upper):
	if p==isprime(p):
		primes.append(p)
endt=time.process_time()
print("Probedivision, nicht optimiert:")
print("%u Primzahlen gefunden in %.2f s" % (len(primes),endt-startt))
print("-"*76)

# Durchlauf mit optimierter Klasse
primes=[]
startt=time.process_time()
check = Primes()
check.debug == True
#for p in range(upper,lower,-1):
for p in range(lower,upper):
	# Divisonstest
	if check.isPrime(p)==True:
		primes.append(p)

endt=time.process_time()
print("Probedivision, optimiert:")
print("%u Primzahlen gefunden in %.2f s" % (len(primes),endt-startt))
print("-"*76)

# zweiter Durchlauf
primes=[]
startt=time.process_time()
#for p in range(upper,lower,-1):
for p in range(lower,upper):
	# Divisonstest
	if check.isPrime(p)==True:
		primes.append(p)

endt=time.process_time()
print("Probedivision, optimiert, zweiter Durchlauf:")
print("%u Primzahlen gefunden in %.2f s" % (len(primes),endt-startt))
print("-"*76)

# -- EOF -------------------------------------------------------------------
